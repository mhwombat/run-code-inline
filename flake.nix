{
  description = "execute code in text files";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        packages = rec {
          run-code-inline = pkgs.stdenv.mkDerivation rec {
            name = "run-code-inline";

            src = ./.;

            unpackPhase = "true";

            buildPhase = ":";

            installPhase =
              ''
                mkdir -p $out/bin
                cp $src/run-code-inline $out/bin/
                chmod +x $out/bin/run-code-inline
              '';
          };
          default = run-code-inline;
        };

        apps = rec {
          run-code-inline = flake-utils.lib.mkApp { drv = self.packages.${system}.run-code-inline; };
          default = run-code-inline;
        };
      }
    );
}
